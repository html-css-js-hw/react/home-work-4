export const setProductList = (products) => ({
    type: 'SET_PRODUCT_LIST',
    payload: products,
});

export const toggleFavorite = (product) => ({
    type: 'TOGGLE_FAVORITE',
    payload: { product },
});

export const addToCart = (product) => ({
    type: 'ADD_TO_CART',
    payload: { product },
});

export const removeFromCart = (product) => ({
    type: 'REMOVE_FROM_CART',
    payload: { product },
});

export const SET_PRODUCT_LIST = 'SET_PRODUCT_LIST';
export const TOGGLE_FIRST_MODAL = 'TOGGLE_FIRST_MODAL';
export const TOGGLE_REMOVE_MODAL = 'TOGGLE_REMOVE_MODAL';
export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
